section .text

%include "lib.inc"

global find_word

find_word: 
.cycle:
	push rdi;rdi, rsi регистры сохранютяс ВЫЗЫВАЮЩЕЙ функцией
	push rsi
	add rsi, 8
	call string_equals ;сравнили строки
	pop rsi;спокойно достаём обратно наши аргументы
	pop rdi
	cmp rax, 1
	je .exist ;если они равны - переходим в конец
	mov rsi, [rsi]; стучимся до того самого end из макроса
	test rsi, rsi;в ином случае проверяем, последний ли это элемент
	jz .not_exist; если последний - значит ничего не найдено
	jne .cycle;иначе продолжаем поиски
.not_exist:
	xor rax, rax
	ret
.exist:
	mov rax, rsi
	ret
