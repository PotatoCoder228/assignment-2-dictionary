section .rodata

%include "words.inc"

%define buffer_size 256
%define stdout_cal 1

message:
	db "Input searching word:", 10, 0

not_find:
	db 10, "Nothing is found!", 0

error: 
	db 10, "This key is not found!", 0

section .text

%include "lib.inc"

extern find_word

global _start

_start:
	sub rsp, buffer_size
	mov rsi, buffer_size ; задали размер буфера, в который будем читать
	mov rdi, rsp
	push rdi 
	call read_word ; прочитали слово
	test rax, rax ; проверка на то, было ли слово введено
	jz .good_bye_err ; если нет - выход
	pop rdi ; со спокойной душой достаём и идём дальше
	mov rsi, end ; кладём адрес верхнего ключа словаря
	call find_word ; ищем по ключу в словаре
	test rax, rax ; если 0, то ничего не найдено
	jz .good_bye_not_find
	jmp .good_bye_ok



.good_bye_err:
	call print_err
	call exit

.good_bye_not_find:
	mov rdi, not_find
	call print_string
	call exit

.good_bye_ok:
	add rax, 8
	push rax
	mov rsi, rax
	call string_length
	pop rsi
	add rax, rsi
	inc rax
	push rax
	pop rdi
	mov rdx, 1
	call print_string
	call print_newline
	add rsp, buffer_size
	xor rdi, rdi
	call exit

print_err:
	mov rdi, error
	call string_length
	mov rdx, rax
   	mov rax, stdout_cal
   	mov rsi, error
   	mov rdi, 2
   	syscall
   	ret

